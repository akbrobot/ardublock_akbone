package com.ardublock.translator.block;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

public class LEDPANEL_SETTEXTSPRITE extends TranslatorBlock
        {

                public LEDPANEL_SETTEXTSPRITE(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
                {
                        super(blockId, translator, codePrefix, codeSuffix, label);
                }

                @Override
                public String toCode() throws SocketNullException, SubroutineNotDeclaredException
                {

                TranslatorBlock tb = this.getRequiredTranslatorBlockAtSocket(0);
                String sprite = tb.toCode();
		sprite=sprite.substring(1,sprite.length()-1); // ダブルクォーテーションを取り除く

                tb = this.getRequiredTranslatorBlockAtSocket(1);
                String font = tb.toCode();
		font=font.substring(1,font.length()-1); // ダブルクォーテーションを取り除く
                tb = this.getRequiredTranslatorBlockAtSocket(2);
                String r = tb.toCode();
                tb = this.getRequiredTranslatorBlockAtSocket(3);
                String g = tb.toCode();
                tb = this.getRequiredTranslatorBlockAtSocket(4);
                String b = tb.toCode();

                String ret = 
	sprite+".setFont(&fonts::"+font+");\n"
+	sprite+".setTextColor("+sprite+".color888("+r+","+g+","+b+"));\n";

		return ret;
	        }	

        }


