package com.ardublock.translator.block;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

public class LEDPANEL_CREATESPRITE extends TranslatorBlock
        {

                public LEDPANEL_CREATESPRITE(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
                {
                        super(blockId, translator, codePrefix, codeSuffix, label);
                }

                @Override
                public String toCode() throws SocketNullException, SubroutineNotDeclaredException
                {

                TranslatorBlock tb = this.getRequiredTranslatorBlockAtSocket(0);
                String sprite = tb.toCode();
		sprite=sprite.substring(1,sprite.length()-1); // ダブルクォーテーションを取り除く
                tb = this.getRequiredTranslatorBlockAtSocket(1);
                String x = tb.toCode();
                tb = this.getRequiredTranslatorBlockAtSocket(2);
                String y = tb.toCode();


                translator.addDefinitionCommand("static LGFX_Sprite "+sprite+"(&display);");
                translator.addSetupCommand(sprite+".createSprite("+x+","+y+");");
		String ret="";
		return ret;
	        }	

        }


