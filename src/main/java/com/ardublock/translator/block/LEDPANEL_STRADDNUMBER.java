package com.ardublock.translator.block;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

public class LEDPANEL_STRADDNUMBER extends TranslatorBlock
{
	public LEDPANEL_STRADDNUMBER(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException
	{
		String ret = "String(";

                TranslatorBlock tb = this.getRequiredTranslatorBlockAtSocket(0);
                ret = ret + tb.toCode();
                ret = ret + ") + ";
                tb = this.getRequiredTranslatorBlockAtSocket(1);
                ret = ret + tb.toCode();
		return ret;
	}

}


